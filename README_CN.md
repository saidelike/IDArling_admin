# IDArling后台管理系统

![IDArling Logo](public/static/avatar.png "IDArling Logo")

## 目录结构


| Location             |  Content                                   |
|----------------------|--------------------------------------------|
| `/backend`           | Django 项目目录                             |
| `/backend/api`       | IDArling Server REST API 的 Wrapper 实现      |
| `/src`               | Vue App                                    |
| `/public/static`     | 静态资源                                   |
| `/dist/`             | 编译打包生成的文件                         |

## 调试环境安装方法

### 环境

管理系统基于下面的各种软件开发而成，需要安装这些环境:

- [X] Yarn - [instructions](https://yarnpkg.com/en/docs/install)
- [X] Vue CLI 3 - [instructions](https://cli.vuejs.org/guide/installation.html)
- [X] Python 3 - [instructions](https://wiki.python.org/moin/BeginnersGuide)
- [X] Pipenv - [instructions](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)

### 安装
```
$ yarn install
$ pipenv install --dev && pipenv shell
$ python manage.py migrate
```

### 运行

运行 Django 后端服务器

```
$ python manage.py runserver
```

在另一个终端运行 vuejs 的服务器

```
$ yarn serve
```

Vue 的服务器会监听在[`localhost:8080`](http://localhost:8080/)，Django API 则会监听在[`localhost:8000`](http://localhost:8000/)

也可以编译打包后运行
```
$ yarn build
$ python manage.py runserver
```
## Pycharm 附加配置

参阅官方文档：https://www.jetbrains.com/help/pycharm/pipenv.html

点击 "Edit Configurations"

选择 Django Server under templates

点击 + 来创建一个 Django 配置，在环境变量中填入

```
PYTHONUNBUFFERED=1;DJANGO_SETTINGS_MODULE=backend.settings.dev
```

点击确定即可。