import Cookies from 'js-cookie'

const LOGIN_COOKIE_KEY = 'idarling_manage_is_login'

export function isLogin() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
