import request from '@/utils/request'

export function list_permission(user, nodeid, permtype) {
  return request({
    url: '/permission/list_permission',
    method: 'post',
    data: {user: user, nodeid: nodeid, permtype: permtype}
  })
}

export function add_permission(username, nodeid, permtype) {
  return request({
    url: '/permission/add_permission',
    method: 'post',
    data: {user: username, nodeid: nodeid, permtype: permtype}
  })
}

export function remove_permission(username, nodeid, permtype) {
  return request({
    url: '/permission/remove_permission',
    method: 'post',
    data: {user: username, nodeid: nodeid, permtype: permtype}
  })
}