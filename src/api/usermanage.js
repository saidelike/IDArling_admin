import request from '@/utils/request'

export function get_users(params) {
  return request({
    url: '/usermanage/get_users',
    method: 'get',
    params
  })
}

export function add_user(username, password, pubkey) {
  return request({
    url: '/usermanage/add_user',
    method: 'post',
    data: {"user": username, "password": password, "pubkey": pubkey}
  })
}

export function del_user(username) {
  return request({
    url: '/usermanage/del_user',
    method: 'post',
    data: {user: username}
  })
}

export function update_user(username, password, pubkey) {
  return request({
    url: '/usermanage/update_user',
    method: 'post',
    data: {user: username, password: password, pubkey: pubkey}
  })
}