from django.contrib.auth.base_user import AbstractBaseUser

from backend.api.models import IDArlingUser
from .idarling_settings import *

import requests
'''
class IDArlingUser(object):
    class Meta(object):
        class PK(object):
            def __init__(self, username):
                self.value_to_string = lambda _ : username
                self.to_python = lambda username: IDArlingUser 
        def __init__(self, username):
            self.pk = self.PK(username)

    def __init__(self, username):
        self.objects = None
        self.username = username
        self._meta = self.Meta(username)

    def save(self, *args, **kwargs):
        pass
'''

class IDArlingBackend:
    def authenticate(self, request, username=None, password=None):
        # Check the username/password and return a user.
        r = requests.post(IDARLING_AUTHENTICATE, json={"user": username, "pass": password})
        if r.json()["status"] == "ok":
            user = IDArlingUser(username=username) # stub user
            user.save()
            return user
        else:
            return None

    def get_user(self, userid):
        return IDArlingUser(username=userid)