"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from .api.views import index_view, user_login, get_users, list_tree, list_permission, loginstatus, remove_permission, \
    add_permission, add_user, del_user, update_user

router = routers.DefaultRouter()

apiurls = [
    path('user/login', user_login, name='login'),
    path('user/loginstatus', loginstatus, name='loginstatus'),

    path('usermanage/get_users', get_users, name='get_users'),
    path('usermanage/add_user', add_user, name='add_user'),
    path('usermanage/del_user', del_user, name='del_user'),
    path('usermanage/update_user', update_user, name='update_user'),

    path('projmanage/list_tree', list_tree, name='list_tree'),

    path('permission/list_permission', list_permission, name='list_permission'),
    path('permission/add_permission', add_permission, name='add_permission'),
    path('permission/remove_permission', remove_permission, name='add_permission'),

]

urlpatterns = [

    # http://localhost:8000/
    path('', index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    #path('api/', include(router.urls)),
    path('api/', include(apiurls)),

    # http://localhost:8000/api/admin/
    path('api/admin/', admin.site.urls),
]


