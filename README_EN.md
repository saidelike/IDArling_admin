This is an English version of the [README.md](README.md) documentation.

# IDArling management web server

![IDArling Logo](public/static/avatar.png "IDArling Logo")

## Directory Structure

| Location             |  Content                                   |
|----------------------|--------------------------------------------|
| `/backend`           | Django project directory |
| `/backend/api`       | Wrapper implementation of IDArling Server REST API |
| `/src`               | Vue App                                    |
| `/public/static`     | Static Resources |
| `/dist/`             | Compile and package the generated files |

## Installation

Here we provide two installation methods, docker automatic installation and manual installation. It is recommended to use the docker installation that installs both the IDArling server (idarling_server) and the IDArling management web server (idarling_admin) automatically.

### docker installation

Refer to https://gitlab.com/saidelike/IDArling_deploy/README_EN.md

### Manual installation

#### Dependencies

The management web server is developed based on the following various software, and these environments need to be installed first following their respectives instructions:

- NodeJS - [instructions](https://github.com/nodesource/distributions/blob/master/README.md)
- Yarn - [instructions](https://yarnpkg.com/en/docs/install)
- Vue CLI 3 - [instructions](https://cli.vuejs.org/guide/installation.html)
- Python 3 - [instructions](https://wiki.python.org/moin/BeginnersGuide)
- Pipenv - [instructions](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)

We use a Python environment to avoid corrupting an existing Python installation.

We only detail dependencies installation on Ubuntu.

##### Dependencies on Ubuntu

Install NodeJS:

```
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Install Yarn:

```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
```

Install Pipenv:

```
sudo -H pip3 install --user pipenv
```

Install Vue CLI 3

```
yarn global add @vue/cli
```

#### Installing the management web server

It is recommended to execute it in a `screen`.

```
yarn install
pipenv install --dev && pipenv shell
pip3 install whitenoise django djangorestframework requests
cd idarling_admin
python manage.py migrate
```

To execute the `pipenv` command on Ubuntu, you may have to first execute:

```
export LC_ALL=C.UTF-8
export LANG=C.UTF-8
```

#### Starting the management web server

Run the Django frontend server:

```
python manage.py runserver 0.0.0.0:8000
```

A server running vuejs in another terminal (this seems to be optional?):

```
cd idarling_admin
yarn serve
```

The Vue server will listen on [`localhost:8080`] (http://localhost:8080/), and the Django API will listen on [`localhost:8000`] (http://localhost:8000/)

If you need to rebuild the web application, you can run:

```
yarn build
```

Then you can restart the server as usual:

```
python manage.py runserver 0.0.0.0:8000
```

## Pycharm additional configuration

Refer to the official documentation: https://www.jetbrains.com/help/pycharm/pipenv.html

Click "Edit Configurations"

Select Django Server under templates

Click + to create a Django configuration, fill in the environment variables

```
PYTHONUNBUFFERED=1;DJANGO_SETTINGS_MODULE=backend.settings.dev
```

Click OK.